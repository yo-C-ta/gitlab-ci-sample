package fizzbuzz

import (
	"fmt"
)

// FizzBuzz is hogehoge
func FizzBuzz(size int) (fb string) {
	for i := 1; i <= size; i++ {
		result := ""
		if i%3 == 0 {
			result += "Fizz"
		}
		if i%5 == 0 {
			result += "Buzz"
		}
		if result != "" {
			fb += fmt.Sprintf("%s\n", result)
		} else {
			fb += fmt.Sprintf("%d\n", i)
		}
	}
	return
}
