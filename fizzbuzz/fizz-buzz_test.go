package fizzbuzz

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func main() {
	tests := []testing.InternalTest{{"TestFizzBuzz", TestFizzBuzz}}
	matchAll := func(t string, pat string) (bool, error) { return true, nil }
	testing.Main(matchAll, tests, nil, nil)

}

func TestFizzBuzz(t *testing.T) {
	actual := FizzBuzz(15)
	expected :=
		`1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
`
	assert.Equal(t, expected, actual)
}
