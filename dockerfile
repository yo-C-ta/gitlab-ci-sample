FROM golang:latest AS build-env

ADD . /go/src/gitlab.com/yo-C-ta/gitlab-ci-sample
WORKDIR /go/src/gitlab.com/yo-C-ta/gitlab-ci-sample
RUN go build -o ./dst/fizzbuzz main.go


FROM alpine

COPY --from=build-env /go/src/gitlab.com/yo-C-ta/gitlab-ci-sample/dst/fizzbuzz /fizzbuzz
ENTRYPOINT ["/fizzbuzz"]
