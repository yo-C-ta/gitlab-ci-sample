package main

import (
	"fmt"
	"time"

	"gitlab.com/yo-C-ta/gitlab-ci-sample/fizzbuzz"
)

func main() {
	start := time.Now()
	fmt.Println(fizzbuzz.FizzBuzz(10000))
	fmt.Printf("%f sec\n", time.Now().Sub(start).Seconds())
}
